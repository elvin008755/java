package Problems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PrOne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] sample = new String[8];
        char[] letter = new char[8];
        String[] num = new String[] { "", "", "", "", "", "", "", "" };
        int[] tStore = new int[8];
        char temp = ' ';
        int start = 0;
        int end = 0;
        String flist = "";
        List<String> s = new ArrayList<String>();
        s.add("c10");
        s.add("a10");
        s.add("a1");
        s.add("a2");
        s.add("b2");
        s.add("b100");
        s.add("b3");
        s.add("b99");
        Collections.sort(s);

        for (int i = 0; i < s.size(); i++)
        {
            sample[i] = s.get(i).toString();
        }

        for (int j = 0; j < sample.length; j++)
        {
            for (int k = 0; k < sample[j].length(); k++)
            {

                if (Character.isLetter(sample[j].charAt(k))){
                	
                  letter[j] = sample[j].charAt(k);

                }
                if (Character.isDigit(sample[j].charAt(k)))
                {
                    num[j] += sample[j].charAt(k);
                }
            }
        }

        for (int p = 0; p < s.size(); p++)
        {

            if (p == 0)
            {
                tStore[p] = Integer.parseInt(num[p]);
                start = 0;
                temp = letter[p];
            }

            if (p > 0)
            {

            	
                if (temp == letter[p])
                {
                    tStore[p] = Integer.parseInt(num[p]);
                }
                else if (temp != letter[p])
                {
                    end = p;
                    temp = letter[p];
                    tStore[p] = Integer.parseInt(num[p]);
                    for (int i = start; i < end; i++)
                    {
                        for (int j = i + 1; j < end; j++)
                        {
                            int x;
                            if (tStore[i] > tStore[j])
                            {
                                x = tStore[i];
                                tStore[i] = tStore[j];
                                tStore[j] = x;
                            }
                        }
                    }
                    start = end;
                }
            }

        }

        for (int k = 0; k < tStore.length; k++)
        {
            flist += letter[k];
            flist += tStore[k];
            sample[k] = flist;
            System.out.println(sample[k]);
            flist = "";
        }
	}

}
